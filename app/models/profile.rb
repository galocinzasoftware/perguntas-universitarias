class Profile < ActiveRecord::Base
  has_merit

  attr_accessible :name, :profile_image_url, :birthdate, :website, :user_id

  belongs_to :user

  validates_uniqueness_of :user_id
end

class Question < ActiveRecord::Base
  attr_accessible :title, :body, :tag_list, :user_id
  belongs_to :profile
  has_many :comments, dependent: :destroy
  has_many :answers, dependent: :destroy
  acts_as_taggable

  validates_uniqueness_of :title, :case_sensitive => false

  extend FriendlyId
  friendly_id :title, use: [:slugged, :history]
end

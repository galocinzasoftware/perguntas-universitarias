class ApplicationController < ActionController::Base
  protect_from_forgery

  def current_profile
    @current_profile ||= current_user.profile
  end
end

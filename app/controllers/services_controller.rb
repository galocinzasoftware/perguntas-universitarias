class ServicesController < Devise::OmniauthCallbacksController

  def index
    @services = current_user.services.all
  end

  def destroy
    @service = current_user.services.find(params[:id])
    @service.destroy

    redirect_to services_path
  end

  def facebook
    omniauth = request.env['omniauth.auth']

    if omniauth
      email    = omniauth['extra']['raw_info']['email'] ? omniauth['extra']['raw_info']['email'] : ''
      name     = omniauth['extra']['raw_info']['name'] ? omniauth['extra']['raw_info']['name'] : ''
      username = omniauth['extra']['raw_info']['username'] ? omniauth['extra']['raw_info']['username'] : ''
      uid      = omniauth['extra']['raw_info']['id'] ? omniauth['extra']['raw_info']['id'] : ''
      provider = omniauth['provider'] ? omniauth['provider'] : ''

      save_service(email, username, name, uid, provider)
    else
      flash[:error] = I18n.t('devise.omniauth_callbacks.failure', :kind=>'Facebook')
      redirect_to new_user_session_path
    end
  end

  def google_oauth2
      omniauth = request.env['omniauth.auth']

      if omniauth
        email    = omniauth['extra']['raw_info']['email'] ? omniauth['extra']['raw_info']['email'] : ''
        name     = omniauth['extra']['raw_info']['name'] ? omniauth['extra']['raw_info']['name'] : ''
        username = omniauth['extra']['raw_info']['username'] ? omniauth['extra']['raw_info']['username'] : ''
        uid      = omniauth['extra']['raw_info']['id'] ? omniauth['extra']['raw_info']['id'] : ''
        provider = omniauth['provider'] ? omniauth['provider'] : ''

        save_service(email, username, name, uid, provider)
      else
        flash[:error] = I18n.t('devise.omniauth_callbacks.failure', :kind=>'Facebook')
        redirect_to new_user_session_path
      end
  end

  private

  def save_service (email, username, name, uid, provider)
    # continue only if provider and uid exist
      if uid != '' and provider != ''
          
        # nobody can sign in twice, nobody can sign up while being signed in (this saves a lot of trouble)
        if !user_signed_in?
          
          # check if user has already signed in using this service provider and continue with sign in process if yes
          auth = Service.find_by_provider_and_uid(provider, uid)
          if auth
            flash[:notice] = I18n.t('devise.omniauth_callbacks.success', :kind=>'Facebook')
            sign_in_and_redirect(:user, auth.user)
          else
            # check if this user is already registered with this email address; get out if no email has been provided
            if email != ''
              # search for a user with this email address
              existinguser = User.find_by_email(email)
              if existinguser
                # map this new login method via a service provider to an existing account if the email address is the same
                existinguser.services.create(:provider => provider, :uid => uid, :uname => name, :uemail => email)
                flash[:notice] = 'Sign in via ' + provider.capitalize + ' has been added to your account ' + existinguser.email + '. Signed in successfully!'
                sign_in_and_redirect(:user, existinguser)
              else
                # new user, set email, a random password and take the name from the authentication service
                user = User.new(:email => email, :username => username,:password => SecureRandom.hex(10), :has_local_password => false)
                # add this authentication service to our new user
                user.services.build(:provider => provider, :uid => uid, :uname => name, :uemail => email)
                user.build_profile(:name => name)

                # do not send confirmation email, we directly save and confirm the new record
                user.skip_confirmation!
               
                user.save!
                user.confirm!
                
                #Send welcome email
                UserMailer.welcome_message(user).deliver

                # flash and sign in
                flash[:notice] = 'Your account has been created via ' + provider.capitalize + '. In your profile you can change your personal information and add a local password.'
                sign_in_and_redirect(:user, user)
              end
            else
              flash[:error] =  provider.capitalize + ' can not be used to sign-up as no valid email address has been provided. Please use another authentication provider or use local sign-up. If you already have an account, please sign-in and add ' + provider.capitalize + ' from your profile.'
              redirect_to new_user_session_path
            end
          end
        else
          # the user is currently signed in
          
          # check if this service is already linked to his/her account, if not, add it
          auth = Service.find_by_provider_and_uid(provider, uid)
          if !auth
            current_user.services.create(:provider => provider, :uid => uid, :uname => name, :uemail => email)
            flash[:notice] = 'Sign in via ' + provider.capitalize + ' has been added to your account.'
            redirect_to services_path
          else
            flash[:notice] = provider.capitalize + ' is already linked to your account.'
            redirect_to services_path
          end  
        end  
      else
        flash[:error] =  provider.capitalize + ' returned invalid data for the user id.'
        redirect_to new_user_session_path
      end
    end
end

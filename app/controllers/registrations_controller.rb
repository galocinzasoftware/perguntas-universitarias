class RegistrationsController < Devise::RegistrationsController

  def update
    if current_user.has_local_password
      super
    else
      # required for settings form to submit when password is left blank
      if params[:user][:password].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      else
        # if the user wants to set a password we set has_local_password for the future
        params[resource_name][:has_local_password] = true
      end
      
      @user = User.find(current_user.id)
      if @user.update_attributes(params[:user])
        set_flash_message :notice, :updated
        # Sign in the user bypassing validation in case his password changed
        sign_in @user, :bypass => true
        redirect_to after_update_path_for(@user)
      else
        render "edit"
      end
    end
  end
end
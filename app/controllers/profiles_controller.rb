class ProfilesController < ApplicationController

  def index
    @profiles = Profile.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @profiles }
    end
  end

  def show
    @profile = Profile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @profile }
    end
  end

  def show_by_username
    #@profile = Profile.joins(:user).where(:user => {:username => params[:username]})
    user = User.find_by_username(params[:username])

    @profile = Profile.find_by_user_id(user)
    respond_to do |format|
      format.html
      format.json { render json: @profile }
    end
  end

  def edit
    if not user_signed_in?
      flash[:error] = I18n.t('devise.failure.unauthenticated')
      redirect_to new_user_session_path
    elsif current_user.id.to_i != params[:id].to_i
      flash[:error] = "You can only edit your own profile."
      redirect_to "/"
    else
      @profile = Profile.find(params[:id])
    end
  end

  def update
    if not user_signed_in?
      flash[:error] = I18n.t('devise.failure.unauthenticated')
      redirect_to new_user_session_path
    elsif current_user.id.to_i != params[:id].to_i
      flash[:error] = I18n.t('devise.failure.unauthenticated')
      redirect_to new_user_session_path
    else
      @profile = Profile.find(params[:id])

      respond_to do |format|
        if @profile.update_attributes(params[:profile])
          format.html { redirect_to @profile, notice: 'Profile was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @profile.errors, status: :unprocessable_entity }
        end
      end
    end
  end
end

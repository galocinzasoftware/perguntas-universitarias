class CommentsController < ApplicationController
  before_filter :load_question

  # GET /comments/1/edit
  def edit
    @comment = current_user.comments.find(params[:id])
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = @question.comments.build(params[:comment])
    @comment.user = current_user
    
    if @comment.save
      redirect_to @question, notice: "Comment was created."
    else
      render :new
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @comment = current_user.comments.find(params[:id])
    if @comment.update_attributes(params[:comment])
      redirect_to @question, notice: "Comment was updated."
    else
      render :edit
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = current_user.comments.find(params[:id])
    @comment.destroy

    redirect_to @question, notice: "Comment was destroyed."
  end

  private

  def load_question
    @question = Question.find(params[:question_id])
  end
end

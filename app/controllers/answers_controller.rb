class AnswersController < ApplicationController
  before_filter :load_question

  # GET /comments/1/edit
  def edit
    @answer = current_user.answers.find(params[:id])
  end

  # POST /comments
  # POST /comments.json
  def create
    @answer = @question.answers.build(params[:answer])
    @answer.user = current_user
    
    if @answer.save
      redirect_to @question, notice: "Answer was created."
    else
      render :new
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @answer = current_user.answers.find(params[:id])
    if @answer.update_attributes(params[:answer])
      redirect_to @question, notice: "Answer was updated."
    else
      render :edit
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @answer = current_user.answers.find(params[:id])
    @answer.destroy

    redirect_to @question, notice: "Answer was destroyed."
  end

  def load_question
    @question = Question.find(params[:question_id])
  end
end

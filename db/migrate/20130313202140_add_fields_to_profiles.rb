class AddFieldsToProfiles < ActiveRecord::Migration
  def self.up
    add_column :profiles, :sash_id, :integer
    add_column :profiles, :level, :integer, :default => 0
  end

  def self.down
    remove_column :profiles, :sash_id
    remove_column :profiles, :level
  end
end

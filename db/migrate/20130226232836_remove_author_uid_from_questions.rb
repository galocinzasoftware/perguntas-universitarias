class RemoveAuthorUidFromQuestions < ActiveRecord::Migration
  def up
	remove_column :questions, :author_uid
  end

  def down
	remove_column :questions, :author_uid
  end
end

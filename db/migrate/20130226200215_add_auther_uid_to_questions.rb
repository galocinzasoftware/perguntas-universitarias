class AddAutherUidToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :author_uid, :string
  end
end

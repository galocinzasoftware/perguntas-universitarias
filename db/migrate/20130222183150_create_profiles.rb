class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string  :name
      t.string  :profile_image_url
      t.date    :birthdate
      t.string  :website

      t.timestamps
    end
  end
end

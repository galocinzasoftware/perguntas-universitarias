class AddHasLocalPasswordToUsers < ActiveRecord::Migration
  def change
    add_column :users, :has_local_password, :boolean, :null => false, :default => true
  end
end

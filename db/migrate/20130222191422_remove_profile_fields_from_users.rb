class RemoveProfileFieldsFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :name
    remove_column :users, :image
    remove_column :users, :university
    remove_column :users, :course
    remove_column :users, :semester
  end

  def down
    add_column :users, :name, :string
    add_column :users, :image, :string
    add_column :users, :university, :string
    add_column :users, :course, :string
    add_column :users, :semester, :string
  end
end

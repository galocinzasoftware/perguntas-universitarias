# Perguntas Universitárias

A Question and Answears (Q&A) software for college students.

## Prerequisites

* Ruby 1.9.3
* Rails 3.2.11

> Use [RVM](https://rvm.io/) to maintain your Ruby and Rails installations.
> If you don't know how to install RVM, just read
> [this tutorial](http://ryanbigg.com/2010/12/ubuntu-ruby-rvm-rails-and-you/) or
> [this (in Portuguese)](http://www.lucaskreutz.com.br/blog/2012/08/20/instalando-ruby-on-rails-no-ubuntu-com-o-rvm/)

## Installation Steps

1. run `bundle install`
2. run `rake db:create:all`
3. run `rake db:migrate`

## Development Rules

* Use [Sublime Text 2](http://www.sublimetext.com/2).
* Write code (this include the docs) in English.
* Commit little steps, little modifications.
* Write automated tests before programming. TDD is welcome.
* Commit only code that works, that is, that pass in all the tests.

## Code Conventions

### Sublime Configuration

In sublime, click in **Projects > Close Project** to close your currently open project.
Then, click **Projects > Add Folder to Project** and select the folder you have
cloned this app. Finally, click **Projects > Save Project As** and save the
project files in the folder you have cloned this app. If you want to open another project,
just use the **Projects > Switch Project in Window** feature to open other project file.

Having done that, change your **.sublime-project** file adding the following settings:

    {
      "folders":
      [
        {
          "path": "THE ABSOLUTE PATH TO YOUR PROJECT FOLDER, DO NOT CHANGE THIS"
        }
      ],
      "settings":
      {
        "tab_size": 2,
        "translate_tabs_to_spaces": true
      }
    }

If you don't want to create a project (oh, boy, why?), just set the *tab_size* to *2*
and *translate_tabs_to_spaces* to *true* in the menu configuration via
**Preferences > Settings - Default**.
